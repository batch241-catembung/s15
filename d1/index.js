console.log("hello world jake ");

// sy

// (ctr /) it will comments part of the code that gets ignore by the language, single line 


/*
	(ctrl shift /) multi comment

*/


// section, syntax and statements
	// statements in programming are instruction that we tell teh computer to perform
	// js statements end with semicolon (;)
	// semicolons are not required but we will use it to help us train to locate where statements end
	// syntax in programming list the set of the rules that we describe statements must be constructed 
	// all lines/blocks  of code should bbe written in specific manner to work. This is due to how these codes where initially programmed to function in a certain manner.


// variables
	// variables are used to contain data
	// any information that is used by an application is stored in what we call the memory
	// when we create variables, certain portion of devices memory is given a name that we call variables
	
	// this make us easir for us to associate information astored in our devices to actual "names" information
	
	// declare variables
	// declaring variables - it tells our devices that a variable name is created and is ready to stored data.
		// syntax
			// let/const variableName;

		let myVariable = "Ada Lovelace";
		let variable;
		// this will caused an undefined variable because the declared variable does not have initial value
		console.log(variable)
		// const keyword use when the value of the variable wont't change.
		const constVariable = "John Doe";
		// console.log is useful for printing values of variables or certain result of code into the browser console
		console.log(myVariable);


		/*
			guides in  writting variables 
				1. use the let keyword followed the variable name of your choose and use the assignment operator (=) to assign value.
				2. variable names should start with lowercase character, use camelCasing for the multible words.
				3.for constant variables, use the 'const keyword'
					note: if we used the const keyword in  declaring a variable, we cannot change the value to its variable.
				4. varibale names should be indicative (descriptive) of the value eeing stored to avoid confusion.
		*/

// declare and initialize
	// initializing variables -the instance when the variable is given its first inital value 
	// syntax
		// let/const variableName =initial value;
	// declaraion adn initialization of the variable occur
	let productName ="desktop computer";
	console.log(productName);
	// declaration
	let desktopName;
	console.log("desktopName");
	// initialization of the value of variable desktopName
	desktopName = "Dell";

	// reassigning value
		// syntax: variableName = newValue
		productName = "Personal computer";
		console.log(productName);
		// this reasign will cause an error since we cannot change/reassign the inital value of constant variable
		const name ="jake";
		console.log(name);
		/*name = "lexter";
		console.log(name);*/

		// this will cause an error on our code because teh productName variable is already taken
		/*let productName = "laptop"*/

		// var vs let/const
			// some of you will wonder we used let a nad const keyword in declaring a variable when we search online, we usually see var.

			// var - isa also used in declaring varible bit var is ecmaScrpit1 feature [(1997)]
		
		let lastName;
		lastName = "catembung"
		console.log(lastName);

/*
		using var, bad practice.
		batch = "batch 241";
		var batch;
		console.log(batch);*/

//////////////////////////////////
// let/const local/global scope
/*
	scope essentially means where these variables are available for use only

	le/const are block scope

	a block is chuck of code bounded by {}.
	
*/


/*let outerVariable ="hello";
console.log(outerVariable);
{

	let innerVariable = "Hello Again";
	console.log(innerVariable);
}


*/

/*const outerVariable ="hello";
console.log(outerVariable);
{

	const innerVariable = "Hello Again";
	console.log(innerVariable);

}
*/

/*var outerVariable ="hello";

{

	var innerVariable = "Hello Again";
	
	
}

console.log(outerVariable);
console.log(innerVariable);*/

// multiple variable declaratio
		let productCode = "DC017", productBrand = "Dell";
	console.log(productCode);
	console.log(productBrand);


// data types
// string
/*
	string are a series of characters that create word, a phrase a sentence anor anything releated to creating a text
	string in javascript ca nbe written using a single ('') or ("") qoute
	
*/
let country = "Philippines";
let province ='Metro Manila';
console.log(country);
console.log(province);

// concatinate string 
// multiple string values can be combined ato create a single string using the "+" symbol

let fullAdress = province + ', ' + country;
	console.log(fullAdress);

let greeting = "i live in the " + country;
console.log(greeting);


let message = "john \'s employee went home  \\ early";
console.log(message);

// "\n" - refers  to creating a new line in between  text

let mailAdress = "metro manila \n\nphilippines";
console.log(mailAdress);

//numbers
// integer/wholenumber
let headCount = 27;
console.log(headCount);
console.log(typeof headCount);

// decimal
let grade = 98.7;
console.log(grade);
console.log(typeof grade);

// exponential notation
let planetDistance = 2e10;
console.log(planetDistance);
console.log(typeof planetDistance);

// string
let planetDistance1 = "2e10";
console.log(planetDistance1);
console.log(typeof planetDistance1);

console.log(typeof grade);
// combining text and strings
console.log("john last quarter grade is " + grade);


// boolean
// true or false
let isMarried = false;
let goodConduct = true;

console.log(typeof isMarried);
console.log( isMarried);

console.log(typeof goodConduct);
console.log( goodConduct);


// arrays
// arrays are special kind of data types that use to store multiple values 

/*
	syntax:
		let/const arrayName = [elementA, elementB... ];

*/
// similar datatypes
let grades = [98, 92.1, 90.1, 94.7];
console.log(grades);
//array is a special kind of object
console.log(typeof grades);


// different datatype
// storing different data types inside array is not recommended
let details = ["john", 32, true];
console.log(details);	

// objects 
// objects are another special kind of data type that's used to mimic real world objcets/ items

/*
	syntax: 
		let/const objectName ={
			propertyA: value;
			propertyb: value;

		}

*/

let person = {
	firstName: "john",
	lastName: "smith",
	age: 32,
	isMarried: false,
	contact: ["+639999999", "8989-898-909" ],
	adress:{
		houseNumber: "345",
		city : "manila"
	}

}
console.log(person);


let person1 = {};
console.log(person1);



//
let myGrades ={
	firstgrading: 90,
	secondgrading: 92.1,
	thirdgrading: 90.1,
	fourthgrading: 94.7
};

/*
	constant objects and constant array
	
	we can change the elemente of the array to constant variable 
*/

const anime = ["one piece", "one puch man", "your lie in April" ];
console.log(anime);
// arrayName[indexNumber]
anime[0] =["kimetsu no yaiba"];
console.log(anime);


const anime1 = ["one piece", "one puch man", "your lie in April" ];
/*console.log(anime1);
anime1 = ["kimetsu no yaiba"];
console.log(anime1);*/

// null 
// it is used to intentionaly express the abesence of the value in a variable declaration/initialization

let spouse = null;
console.log(spouse);
// undefined
// represent of the variable that has been declared but without an assign value

let inARelationship;
console.log(inARelationship);





